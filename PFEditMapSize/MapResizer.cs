﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using static PFEditMapSize.MapResizer.SizeType;

namespace PFEditMapSize
{
    class MapResizer
    {
        private static SizeData sizeold_tmp;
        private static SizeData sizenow_tmp;
        private static SizeData offset_tmp;

        private static T[] Resize2DArray<T>(T[] oldarr, int oldw, int oldh, int w, int h, int xoff = 0, int yoff = 0)
        {
            /*
            T[] arr = new T[w * h];
            Debug.Log($"{oldw}, {oldh}, {w}, {h}");
            int maxw = Math.Min(oldw, w);
            int maxh = Math.Min(oldh, h);
            for (int y = 0; y < maxh; y++)
            {
                Array.Copy(oldarr, y * oldw, arr, y * w, maxw);
                //for (int x = 0; x < maxw; x++)
                //{
                //    arr[x + y * w] = oldarr[x + y * oldw];
                //}
            }
            return arr;
            //*/

            /*
            T[] arr = new T[w * h];
            Debug.Log($"{oldw}, {oldh}, {w}, {h}");
            //int maxw = Math.Min(oldw, w);
            //int maxh = Math.Min(oldh, h);
            int sstartx = Math.Max(-xoff, 0);
            int sstarty = Math.Max(-yoff, 0);
            int sendx = 0;
            int sendy = 0;
            int tstartx = Math.Max(xoff, 0);
            int tstarty = Math.Max(xoff, 0);
            int tendx = Math.Max(xoff, 0);
            int tendy = Math.Max(xoff, 0);
            for (int y = starty; y < leny; y++)
            {
                Array.Copy(oldarr, y * oldw + startx, arr, (y + yoff) * w + xoff, lenx);
                //Array.Copy(oldarr, y * oldw, arr, y * w, maxw);
            }
            return arr;
            //*/

            T[] arr = new T[w * h];
            int wstart1 = Math.Max(0, -xoff);
            int wstart2 = Math.Max(0, xoff);
            int wlength = Math.Max(0, Math.Min(oldw - wstart1, w - wstart2));
            int hstart1 = Math.Max(0, -yoff);
            int hstart2 = Math.Max(0, yoff);
            int hlength = Math.Max(0, Math.Min(oldh - hstart1, h - hstart2));

            for (int y = 0; y < hlength; y++)
            {
                Array.Copy(oldarr, (y + hstart1) * oldw + wstart1, arr, (y + hstart2) * w + wstart2, wlength);
            }

            return arr;
        }

        private static T[] Resize2DArray<T>(T[] oldarr, SizeData old, SizeData now, SizeData off, SizeType type)
        {
            Tuple<int, int> oldt = old.GetFromType(type);
            Tuple<int, int> newt = now.GetFromType(type);
            Tuple<int, int> offt = off.GetFromType(type);
            return Resize2DArray(oldarr, oldt.x, oldt.y, newt.x, newt.y, offt.x, offt.y);
        }

        private static T[] Resize2DArray<T>(T[] oldarr, SizeType type)
        {
            return Resize2DArray(oldarr, sizeold_tmp, sizenow_tmp, offset_tmp, type);
        }

        private static void Offset2DCoords(ref int x, ref int y, int xoff, int yoff)
        {
            x += xoff;
            y += yoff;
        }

        private static void Offset2DCoords(ref int x, ref int y, SizeData off, SizeType type)
        {
            Tuple<int, int> offt = off.GetFromType(type);
            Offset2DCoords(ref x, ref y, offt.x, offt.y);
        }

        private static void Offset2DCoords(ref int x, ref int y, SizeType type)
        {
            Offset2DCoords(ref x, ref y, offset_tmp, type);
        }

        public enum SizeType
        {
            GAMESPACE,
            GRID,
            LARGEGRID,
            MODULEGRID,
        }

        struct SizeData
        {
            public int w; public int h;
            public int gw; public int gh;
            public int lgw; public int lgh;
            public int mgw; public int mgh;

            public SizeData(bool init)
            {
                w = init ? GameSpace.GAMESPACE_WIDTH : 0;
                h = init ? GameSpace.GAMESPACE_HEIGHT : 0;
                gw = init ? GameSpace.GRID_WIDTH : 0;
                gh = init ? GameSpace.GRID_HEIGHT : 0;
                lgw = init ? GameSpace.LARGEGRID_WIDTH : 0;
                lgh = init ? GameSpace.LARGEGRID_HEIGHT : 0;
                mgw = init ? GameSpace.GRID_MODULEGRIDWIDTH : 0;
                mgh = init ? GameSpace.GRID_MODULEGRIDHEIGHT : 0;
            }

            public Tuple<int, int> GetFromType(SizeType type)
            {
                switch (type)
                {
                    case SizeType.GAMESPACE:
                        return new Tuple<int, int>(w, h);
                    case SizeType.GRID:
                        return new Tuple<int, int>(gw, gh);
                    case SizeType.LARGEGRID:
                        return new Tuple<int, int>(lgw, lgh);
                    case SizeType.MODULEGRID:
                        return new Tuple<int, int>(mgw, mgh);
                }

                return new Tuple<int, int>(-1, -1);
            }

            public void AddRef(SizeData other)
            {
                w += other.w; h += other.h;
                gw += other.gw; gh += other.gh;
                lgw += other.lgw; lgh += other.lgh;
                mgw += other.mgw; mgh += other.mgh;
            }

            public void SubRef(SizeData other)
            {
                w -= other.w; h -= other.h;
                gw -= other.gw; gh -= other.gh;
                lgw -= other.lgw; lgh -= other.lgh;
                mgw -= other.mgw; mgh -= other.mgh;
            }
        }

        public static void ResizeMap(int w, int h, int xoff = 0, int yoff = 0)
        {
            GameSpace space = GameSpace.instance;
            if (!GameSpace.editMode || space == null)
            {
                throw new Exception("Cannot resize map outside of edit mode");
            }

            //int w = gw / GameSpace.GRID_SIZE;
            //int h = gh / GameSpace.GRID_SIZE;

            SizeData old = new SizeData(true);
            GameSpace.ReassignConstants(xoff * GameSpace.GRID_SIZE, yoff * GameSpace.GRID_SIZE);
            SizeData off = new SizeData(true);
            GameSpace.ReassignConstants(w * GameSpace.GRID_SIZE, h * GameSpace.GRID_SIZE);
            SizeData now = new SizeData(true);
            sizeold_tmp = old; sizenow_tmp = now; offset_tmp = off;

            ResizeLand(old, now, off);
            ResizeFields(old, now, off);
            ResizePlasma(old, now, off);
            ResizeStruc(old, now, off);

            OffsetUnits(old, now, off);
            OffsetParticles(old, now, off);
            OffsetPacks(old, now, off);
            OffsetPhasics(old, now, off);
            OffsetModules(old, now, off);
            OffsetLandCollectionEffectPack(old, now, off);

            /*
            MapBorder border = UnityEngine.Object.FindObjectOfType<MapBorder>();
            if(border != null)
            {
                Debug.Log(1);
                Type btype = typeof(MapBorder);
                Debug.Log($"2 {btype}");
                var method = btype.GetMethod("Awake", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                Debug.Log($"3 {method}");
                method.Invoke(border, new object[0]);
                Debug.Log(4);
            }
            //*/

            string text = FileManager.GetEditorBaseDir() + GameSpace.editorDirName + "/resize_tmp_save.pf1";
            SaveGameManager.SaveMission(text, false);
            //SaveGameManager.LoadMission(text, false);
            GameSpace.fileToLoad = text;

            //typeof(GameSpace).GetMethod("Start", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(GameSpace.instance, new object[0]);
            LoadingScreen.LoadGame(text, false, true, GameSpace.launchCategory);
        }


        private static void ResizeLand(SizeData old, SizeData now, SizeData off)
        {
            Land land = land = Land.instance;

            land.landData = Resize2DArray(land.landData, GRID);
            land.landCoveredData = Resize2DArray(land.landCoveredData, GRID);
            //land.targetingLand = Resize2DArray(land.targetingLand, GRID);
            //land.landUnits = Resize2DArray(land.landUnits, GRID);
            //land.packPaths = Resize2DArray(land.packPaths, GRID);
            land.terraformData = Resize2DArray(land.terraformData, GRID);
            //land.terraformBuildData = Resize2DArray(land.terraformBuildData, GRID);

            //land.landCoveredRedLargeData = Resize2DArray(land.landCoveredRedLargeData, LARGEGRID);
            //land.landCoveredBlueLargeData = Resize2DArray(land.landCoveredBlueLargeData, LARGEGRID);
            //land.terraformLargeData = Resize2DArray(land.terraformLargeData, LARGEGRID);
        }

        private static void ResizeFields(SizeData old, SizeData now, SizeData off)
        {
            Fields fields = Fields.instance;

            fields.forceVectorField = Resize2DArray(fields.forceVectorField, GRID);
            //fields.invisibleForceVectorField = Resize2DArray(fields.invisibleForceVectorField, GRID);
            //fields.redModuleForceVectorField = Resize2DArray(fields.redModuleForceVectorField, MODULEGRID);
            //fields.blueModuleForceVectorField = Resize2DArray(fields.blueModuleForceVectorField, MODULEGRID);
        }

        private static void ResizePlasma(SizeData old, SizeData now, SizeData off)
        {
            Plasma plasma = Plasma.instance;

            plasma.terrainData = Resize2DArray(plasma.terrainData, GRID);
            plasma.terrainDecayData = Resize2DArray(plasma.terrainDecayData, GRID);
        }

        private static void ResizeStruc(SizeData old, SizeData now, SizeData off)
        {
            ResizeStruc(GameSpace.instance.redStruc);
            ResizeStruc(GameSpace.instance.blueStruc);
        }

        private static void ResizeStruc(Struc struc)
        {
            struc.strucData = Resize2DArray(struc.strucData, GRID);
            struc.strucHealthData = Resize2DArray(struc.strucHealthData, GRID);
        }

        private static System.Reflection.FieldInfo ReflectionHelperField<T>(string name)
        {
            return typeof(T).GetField(name, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
        }

        private static void OffsetVector3Field<T>(T current, string field, Vector3 off)
        {
            var fieldinfo = ReflectionHelperField<T>(field);
            fieldinfo.SetValue(current, (Vector3)fieldinfo.GetValue(current) + off);
        }

        private static void OffsetVector2Field<T>(T current, string field, Vector2 off)
        {
            var fieldinfo = ReflectionHelperField<T>(field);
            fieldinfo.SetValue(current, (Vector2)fieldinfo.GetValue(current) + off);
        }

        private static void OffsetPatrolCoordinates(List<IntPair> list, Vector2 offset)
        {
            if (list == null) return;
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = new IntPair(list[i].x + (int)offset.x, list[i].y + (int)offset.y);
            }
        }

        private static void OffsetUnits(SizeData old, SizeData now, SizeData off)
        {
            Vector3 offset3 = new Vector3(off.GetFromType(GAMESPACE).x, off.GetFromType(GAMESPACE).y, 0);
            Vector2 goffset2 = new Vector2(off.GetFromType(GRID).x, off.GetFromType(GRID).y);
            HashSet<UnitManager>.Enumerator enumerator = GameSpace.instance.units.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    UnitManager current = enumerator.Current;
                    if (!current.dead /*&& !(current is StructureManager)*/)
                    {
                        //OffsetVector3Field(current, "destination", offset3);
                        var destinationField = ReflectionHelperField<UnitManager>("destination");
                        Vector3 destination = (Vector3)destinationField.GetValue(current);
                        if(destination.x != -1)
                        {
                            destinationField.SetValue(current, destination + offset3);
                        }

                        current.lastGameSpaceX += off.w;
                        current.lastGameSpaceY += off.h;

                        //OffsetVector3Field(current, "position", offset3);
                        current.transform.position += offset3;
                        OffsetVector3Field(current, "lastPosition", offset3);

                        if (current is Doppel)
                        {
                            var doppel = current as Doppel;
                            doppel.homeCell += goffset2;

                            if (doppel.shipBehaviors != null)
                            {
                                var shipBehaviors = doppel.shipBehaviors;

                                shipBehaviors.currentDefendLoc += goffset2;
                                OffsetPatrolCoordinates(shipBehaviors.patrolCoordinates, goffset2);
                            }
                        }

                        if (current is EnergyMine)
                        {
                            var energymine = current as EnergyMine;

                            var deployedStrucX = ReflectionHelperField<EnergyMine>("deployedStrucX");
                            deployedStrucX.SetValue(energymine, (int)deployedStrucX.GetValue(energymine) + off.gw);
                            var deployedStrucY = ReflectionHelperField<EnergyMine>("deployedStrucY");
                            deployedStrucY.SetValue(energymine, (int)deployedStrucY.GetValue(energymine) + off.gh);
                        }

                        if (current is Fighter)
                        {
                            var fighter = current as Fighter;

                            var targetX = ReflectionHelperField<Fighter>("targetX");
                            targetX.SetValue(fighter, (int)targetX.GetValue(fighter) + off.gw);
                            var targetY = ReflectionHelperField<Fighter>("targetY");
                            targetY.SetValue(fighter, (int)targetY.GetValue(fighter) + off.gh);
                        }

                        if (current is LandHarvesterDrone) // Likely unused
                        {
                            var drone = current as LandHarvesterDrone;

                            var targetPixelX = ReflectionHelperField<Fighter>("targetPixelX");
                            targetPixelX.SetValue(drone, (int)targetPixelX.GetValue(drone) + off.w);
                            var targetPixelY = ReflectionHelperField<Fighter>("targetPixelY");
                            targetPixelY.SetValue(drone, (int)targetPixelY.GetValue(drone) + off.h);

                            var targetCellField = ReflectionHelperField<Fighter>("targetCell");
                            int targetCell = (int)targetCellField.GetValue(drone);
                            int cellX = targetCell % old.gh;
                            int cellY = (targetCell - cellX) / old.gh;
                            targetCellField.SetValue(drone, cellX + cellY * now.gh);
                        }

                        if (current is LandShot)
                        {
                            var shot = current as LandShot;

                            OffsetVector2Field(shot, "targetPos", new Vector2(offset3.x, offset3.y));
                        }

                        if (current is LandUnitManager)
                        {
                            var landUnitManager = current as LandUnitManager;

                            var targets = landUnitManager.landMoveTargets;
                            if (targets != null)
                            {
                                for (int i = 0; i < targets.Count; i++)
                                {
                                    var target = targets[i];
                                    if (target == null) continue;

                                    int cellX = target.cell % old.gh;
                                    int cellY = (target.cell - cellX) / old.gh;
                                    target.cell = cellX + cellY * now.gh;
                                }
                            }
                        }

                        if (current is PatrolTokensManager)
                        {
                            var patrolTokensManager = current as PatrolTokensManager;

                            OffsetPatrolCoordinates((List<IntPair>)ReflectionHelperField<PatrolTokensManager>("patrolCoordinates").GetValue(patrolTokensManager), goffset2);
                        }

                        if (current is PhalanxDrone)
                        {
                            var drone = current as PhalanxDrone;

                            var targetX = ReflectionHelperField<PhalanxDrone>("targetX");
                            targetX.SetValue(drone, (int)targetX.GetValue(drone) + off.gw);
                            var targetY = ReflectionHelperField<PhalanxDrone>("targetY");
                            targetY.SetValue(drone, (int)targetY.GetValue(drone) + off.gh);
                            var lastTargetX = ReflectionHelperField<PhalanxDrone>("lastTargetX");
                            lastTargetX.SetValue(drone, (int)lastTargetX.GetValue(drone) + off.gw);
                            var lastTargetY = ReflectionHelperField<PhalanxDrone>("lastTargetY");
                            lastTargetY.SetValue(drone, (int)lastTargetY.GetValue(drone) + off.gh);
                        }
                    }
                }
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }

        }

        private static void OffsetParticles(SizeData old, SizeData now, SizeData off)
        {
            Vector3 offset3 = new Vector3(off.GetFromType(GAMESPACE).x, off.GetFromType(GAMESPACE).y, 0);
            List<Particle>.Enumerator enumerator = GameSpace.instance.particles.particles.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Particle current = enumerator.Current;
                    if (!current.dead)
                    {
                        current.position += offset3;
                        current.lastPosition += offset3;
                    }
                }
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }
        }

        private static void OffsetModules(SizeData old, SizeData now, SizeData off)
        {
            Vector3 offset3 = new Vector3(off.GetFromType(GAMESPACE).x, off.GetFromType(GAMESPACE).y, 0);
            HashSet<Module>.Enumerator enumerator = GameSpace.instance.modules.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Module current = enumerator.Current;

                    current.transform.position += offset3;
                }
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }
        }

        //TODO: Figure out details about LandPacks
        private static void OffsetPacks(SizeData old, SizeData now, SizeData off)
        {
            Vector2 goffset2 = new Vector3(off.GetFromType(GAMESPACE).x, off.GetFromType(GAMESPACE).y, 0);
            HashSet<LandPack>.Enumerator enumerator = GameSpace.instance.landPacks.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    LandPack current = enumerator.Current;

                    var path = ReflectionHelperField<LandPack>("path").GetValue(current) as List<int>;
                    for (int i = 0; i < path.Count; i++)
                    {
                        int targetCell = path[i];
                        int cellX = targetCell % old.gh;
                        int cellY = (targetCell - cellX) / old.gh;
                        path[i] = cellX + cellY * now.gh;
                    }
                }
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }
        }

        private static void OffsetPhasics(SizeData old, SizeData now, SizeData off)
        {
            Vector3 offset3 = new Vector3(off.GetFromType(GAMESPACE).x, off.GetFromType(GAMESPACE).y, 0);
            HashSet<Phasic>.Enumerator enumerator = GameSpace.instance.phasics.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Phasic current = enumerator.Current;
                    current.transform.position += offset3;
                }
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }
        }

        private static void OffsetLandCollectionEffectPack(SizeData old, SizeData now, SizeData off)
        {
            Vector3 offset3 = new Vector3(off.GetFromType(GAMESPACE).x, off.GetFromType(GAMESPACE).y, 0);
            HashSet<LandCollectionEffectPack>.Enumerator enumerator = GameSpace.instance.landCollectionEffectPacks.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    LandCollectionEffectPack current = enumerator.Current;
                    current.transform.position += offset3;
                }
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }
        }
    }
}
