﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SGUI;
using UnityEngine;

namespace PFEditMapSize
{
    class SCustomTextField : SElement
    {
        public string Text
        {
            set
            {
                _text = value;
                if (cursorpos > value.Length) cursorpos = value.Length;
                if (select_length > 0 && cursorpos + select_length > value.Length)
                {
                    select_length = value.Length - cursorpos;
                    if (select_length == 0)
                        select_length = -1;
                }
            }
            get => _text;
        }
        private string _text = "";
        public string SelectedText
        {
            get
            {
                if (HasSelectedText)
                    return _text.Substring(cursorpos, select_length);
                return "";
            }
            set
            {
                if (!HasSelectedText)
                {
                    //decide on behavior
                    select_length = 0;
                }
                int newlength = value.Length;
                _text = _text.Substring(0, cursorpos) + value + _text.Substring(cursorpos + select_length);
            }
        }
        public bool HasSelectedText
        {
            get
            {
                return select_length != -1;
            }
        }

        public Color SelectionColor = new Color(0.2f, 0.5f, 0.9f, 0.5f);
        public Color UnfocusedSelectionColor = new Color(0.4f, 0.4f, 0.4f, 0.6f);

        new public SCustomTextField Next
        {
            set
            {
                var oldnext = _next;
                _next = value;
                if(oldnext != null && oldnext.Previous == this)
                {
                    oldnext.Previous = null;
                }
                if (value != null && value.Previous != this)
                {
                    value.Previous = this;
                }
            }
            get
            {
                return _next;
            }
        }
        new public SCustomTextField Previous
        {
            set
            {
                var oldprev = _previous;
                _previous = value;
                if (oldprev != null && oldprev.Next == this)
                {
                    oldprev.Next = null;
                }
                if(value != null && value.Next != this)
                {
                    value.Next = this;
                }
            }
            get
            {
                return _previous;
            }
        }

        private SCustomTextField _next;
        private SCustomTextField _previous;

        private int cursorpos = 0;
        private int select_length = -1;

        private bool hasfocus = false;
        private bool swapselectionmovement;
        private bool isdragging = false;

        //public static Event LastEventUsed;

        public override void UpdateStyle()
        {
            if (Root == null) return;

            base.UpdateStyle();
        }

        private int GetCharacterIndex(Vector2 pos)
        {
            Vector2 origin = Vector2.zero; (Draw as SGUIIMBackend).PreparePosition(this, ref origin);
            pos -= origin;
            float mx = pos.x;

            int i;
            float tx;
            float lastx = 0;
            for (i = 0; i < _text.Length; i++)
            {
                tx = Draw.MeasureText(_text.Substring(0, i)).x;
                if (tx > mx)
                {
                    //if ((tx + lastx) / 2 > mx)
                    //    i--;
                    break;
                }
                lastx = tx;
            }
            tx = Draw.MeasureText(_text.Substring(0, i)).x;
            if ((tx + lastx) / 2 > mx)
                i--;

            if (i < 0)
                i = 0;

            return i;
        }

        public void GetFocus(Event e = null)
        {
            hasfocus = true;
            if (e != null)
            {
                if (e.type == EventType.mouseDown)
                {
                    cursorpos = GetCharacterIndex(e.mousePosition);
                    select_length = -1;
                }
            }
            else
            {
                cursorpos = 0;
                select_length = _text.Length;
            }
        }

        private void NormalizeSelection()
        {
            if (select_length < 0)
            {
                swapselectionmovement = !swapselectionmovement;
                cursorpos += select_length;
                select_length *= -1;
            }

            int newcursorpos = Math.Max(0, Math.Min(_text.Length, cursorpos));
            //if(swapselectionmovement)
            //{
            //    select_length 
            //}
            select_length = Math.Max(0, Math.Min(_text.Length, cursorpos + select_length)) - cursorpos;
        }

        public override void Update()
        {
            if (!Visible) return;

            Event e = Event.current;

            if(!isdragging && hasfocus && e.isKey && e.type == EventType.KeyDown)
            {
                e.Use();

                //LastEventUsed = e;
                patch_InputManager.HackyInterruptEvent = true;

                KeyCode code = e.keyCode;
                char character = e.character;

                //Debug.Log($"Textbox character pressed: {character}, KeyCode: {code}");

                if(code == KeyCode.Tab || character == '\t')
                {
                    hasfocus = false;
                    select_length = -1;
                    //Debug.Log("Pressed tab");
                    if (_next != null)
                    {
                        //Debug.Log("Jumping to next textbox");
                        _next.GetFocus();
                    }
                    //e.Use();
                }

                if(e.control && code == KeyCode.A)
                {
                    cursorpos = 0;
                    select_length = _text.Length;
                }

                if (code == KeyCode.LeftArrow || code == KeyCode.RightArrow)
                {
                    int off = (code == KeyCode.RightArrow ? 1 : -1);
                    if (e.shift)
                    {
                        if(select_length == -1)
                        {
                            select_length = 0;
                        }
                        if(swapselectionmovement)
                        {
                            cursorpos += off;
                            select_length -= off;
                        }
                        else
                            select_length += off;

                        NormalizeSelection();
                    }
                    else
                    {
                        if(select_length != -1)
                        {
                            cursorpos = (code == KeyCode.RightArrow) ? cursorpos + select_length : cursorpos;
                            select_length = -1;
                        }
                        else
                            cursorpos += off;
                        cursorpos = Math.Max(0, Math.Min(_text.Length, cursorpos));
                    }
                }

                if(code == KeyCode.Backspace && (cursorpos>0 || select_length != -1))
                {
                    if (select_length != -1)
                    {
                        //_text = _text.Substring(0, cursorpos) + _text.Substring(cursorpos + select_length);
                        _text = _text.Remove(cursorpos, select_length);
                        select_length = -1;
                    }
                    else
                    {
                        //_text = _text.Substring(0, cursorpos - 1) + _text.Substring(cursorpos + 1);
                        _text = _text.Remove(cursorpos-1, 1);
                        cursorpos--;
                    }
                }

                if (code == KeyCode.Delete && (cursorpos < _text.Length || select_length != -1))
                {
                    if (select_length != -1)
                    {
                        //_text = _text.Substring(0, cursorpos) + _text.Substring(cursorpos + select_length);
                        _text = _text.Remove(cursorpos, select_length);
                        select_length = -1;
                    }
                    else
                    {
                        //_text = _text.Substring(0, cursorpos - 1) + _text.Substring(cursorpos + 1);
                        _text = _text.Remove(cursorpos, 1);
                    }
                }

                if ((character >= '0' && character <= '9') || character == '-')
                {
                    if (select_length != -1)
                    {
                        //_text = _text.Substring(0, cursorpos) + _text.Substring(cursorpos + select_length);
                        _text = _text.Remove(cursorpos, select_length);
                        select_length = -1;
                    }
                    //_text = _text.Substring(0, cursorpos) + character + _text.Substring(cursorpos + 1);
                    _text = _text.Insert(cursorpos, character.ToString());
                    cursorpos++;
                }
            }

            if(e.isMouse)
            {
                EventType type = e.type;

                if(type == EventType.mouseDown)
                {
                    if(!hasfocus && IsHovered)
                    {
                        hasfocus = true;
                        Debug.Log($"Gained focus");
                    }
                    else if(hasfocus && !IsHovered)
                    {
                        hasfocus = false;
                        select_length = -1;
                        Debug.Log($"Lost focus");
                    }

                    if(hasfocus && IsHovered)
                    {
                        cursorpos = GetCharacterIndex(e.mousePosition);
                        select_length = -1;
                        //Debug.Log($"Intercepted mouse event: {e==LastEventUsed}, {e.GetHashCode()}");
                        //LastEventUsed = e;
                        Debug.Log($"Clicked, cursorpos: {cursorpos}, select_length: {select_length}");
                        patch_InputManager.HackyInterruptEvent = true;
                    }
                }

                if(type == EventType.MouseDrag && (isdragging | (IsHovered && hasfocus)))
                {
                    isdragging = true;
                    if (!HasSelectedText) select_length = 0;
                    int nindex = GetCharacterIndex(e.mousePosition);
                    if(swapselectionmovement)
                    {
                        select_length = select_length - (nindex - cursorpos);
                        cursorpos = nindex;
                    }
                    else
                    {
                        select_length = nindex - cursorpos;
                    }
                    Debug.Log($"Dragging pre-normalize, cursorpos: {cursorpos}, select_length: {select_length}");
                    NormalizeSelection();
                    Debug.Log($"Dragging, cursorpos: {cursorpos}, select_length: {select_length}");
                    //LastEventUsed = e;
                    patch_InputManager.HackyInterruptEvent = true;
                }

                if(type == EventType.MouseUp)
                {
                    isdragging = false;
                }
            }
            base.Update();
        }

        public override void Render()
        {
            Draw.Rect(this, Vector2.zero, Size, Background);
            Draw.Text(this, Vector2.zero, Size, _text, TextAnchor.MiddleLeft);

            float w1 = Draw.MeasureText(_text.Substring(0, cursorpos)).x;
            if(HasSelectedText)
            {
                float w2 = Draw.MeasureText(SelectedText).x;
                Draw.Rect(this, new Vector2(w1, 0), new Vector2(w2, Size.y), hasfocus ? SelectionColor : UnfocusedSelectionColor);
                //Color _Foreground = Foreground;
                //Foreground = Background;
                Draw.Text(this, new Vector2(w1, 0), new Vector2(w2, Size.y), SelectedText, TextAnchor.MiddleLeft);
                //Foreground = _Foreground;
            }
            else if(Time.unscaledTime % 0.8 < 0.4 && hasfocus)
            {
                Draw.Rect(this, new Vector2(w1-0.5f, 0), new Vector2(1, Size.y), Foreground);
            }
        }
    }
}
