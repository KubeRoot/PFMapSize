﻿using MonoMod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PFEditMapSize
{
    [MonoMod.MonoModPatch("global::InputManager")]
    public class patch_InputManager : InputManager
    {
        [MonoModIgnore]
        public patch_InputManager(GameControls gameControls, Camera mainCamera) : base(gameControls, mainCamera) { }

        public static bool HackyInterruptEvent = false;

        public extern void orig_HandleInput();
        public void HandleInput()
        {
            Event e = Event.current;
            //if (SCustomTextField.LastEventUsed == e) return;
            if(HackyInterruptEvent)
            {
                HackyInterruptEvent = false;
                return; // This is ugly, nasty, and everything. Don't do this. Unless you're me and you don't give a damn
            }
            //Debug.Log("HandleInput ran");
            orig_HandleInput();
            //if (GameSpace.editMode)
            //{
                CheckTestingKeyPress();
            //}
        }

        private void CheckTestingKeyPress()
        {
            if (Input.GetKeyDown("k"))
            {
                //Debug.Log("HandleInput detected 'k'");
                UIManager.ToggleResizeMenu();
            }
        }
    }

    [MonoMod.MonoModPatch("global::MainMenu")]
    class patch_MainMenu : MainMenu
    {
        [MonoModIgnore]
        public static extern void orig_cctor_MainMenu();

        [MonoModOriginalName("orig_cctor_MainMenu")]
        [MonoMod.MonoModConstructor]
        public static void cctor_MainMenu()
        {
            orig_cctor_MainMenu();
            //Debug.Log("MainMenu Static Constructor FDSA");
            UIManager.Init();
        }
    }

    [MonoMod.MonoModPatch("global::GameSpace")]
    class patch_GameSpace : GameSpace
    {
        public static extern void orig_LoadGame(string fileToLoad, ProceduralMap proceduralMap, bool embeddedLoad, bool editMode, GameSpace.CATEGORY category, int heapID);
        
        public static void LoadGame(string fileToLoad, ProceduralMap proceduralMap, bool embeddedLoad, bool editMode, GameSpace.CATEGORY category, int heapID)
        {
            //Debug.Log($"LoadGame; editmode: {editMode}, fileToLoad: {fileToLoad}");
            //if (!editMode) UIManager.SetupMenu(false);

            orig_LoadGame(fileToLoad, proceduralMap, embeddedLoad, editMode, category, heapID);

            UIManager.SetupMenu(GameSpace.editMode, GameSpace.GAMESPACE_WIDTH / GameSpace.GRID_SIZE, GameSpace.GAMESPACE_HEIGHT / GameSpace.GRID_SIZE);
        }
    }

    [MonoMod.MonoModPatch("global::MainMenuLoadingScreen")]
    class patch_MainMenuLoadingScreen : MainMenuLoadingScreen
    {
        public extern void orig_Start();
        public void Start()
        {
            UIManager.SetupMenu(false);
            orig_Start();
        }
    }
}
