﻿using SGUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PFEditMapSize
{
    static class UIManager
    {
        static SGUIRoot Root;

        public static SElement ResizePanel;

        private static SCustomTextField textbox_x;
        private static SCustomTextField textbox_y;

        private static SCustomTextField textbox_xoff;
        private static SCustomTextField textbox_yoff;

        private static bool Enabled = false;

        static void InitResizeGUI()
        {
            if (ResizePanel != null) return;
            SGroup group;

            ResizePanel = (group = new SGroup()
            {
                Position = new Vector2(5, 5),
                AutoGrowDirection = SGroup.EDirection.Both,
                ContentSize = new Vector2(0, 0),
                GrowExtra = new Vector2(5, 5),
                Foreground = new Color(0, 0, 0, 0),
                Background = new Color(0.4f, 0.4f, 0.4f, 0.4f),
                IsWindow = true,
                WindowTitle = "Map Resizer",
                Children = {
                    (textbox_x = new SCustomTextField()
                    {
                        Position = new Vector2(5, 5),
                        Size = new Vector2(50, 15),
                        Background = Color.white,
                        Foreground = Color.black,
                        With =
                        {
                            new Tooltip("Size"),
                        }
                    }),
                    new SLabel("x")
                    {
                        Position = new Vector2(57, 5),
                        Size = new Vector2(10, 10),
                        Alignment = TextAnchor.MiddleCenter,
                    },
                    (textbox_y = new SCustomTextField(){
                        Position = new Vector2(65, 5),
                        Size = new Vector2(50, 15),
                        Background = Color.white,
                        Foreground = Color.black,
                        With =
                        {
                            new Tooltip("Size"),
                        },
                        Previous = textbox_x,
                    }),
                    (textbox_xoff = new SCustomTextField()
                    {
                        Position = new Vector2(5, 25),
                        Size = new Vector2(50, 15),
                        Background = Color.white,
                        Foreground = Color.black,
                        Text = "0",
                        With =
                        {
                            new Tooltip("Offset"),
                        },
                        Previous = textbox_y,
                    }),
                    new SLabel("x")
                    {
                        Position = new Vector2(57, 25),
                        Size = new Vector2(10, 10),
                        Alignment = TextAnchor.MiddleCenter,
                    },
                    (textbox_yoff = new SCustomTextField(){
                        Position = new Vector2(65, 25),
                        Size = new Vector2(50, 15),
                        Background = Color.white,
                        Foreground = Color.black,
                        Text = "0",
                        With =
                        {
                            new Tooltip("Offset"),
                        },
                        Previous = textbox_xoff,
                        Next = textbox_x,
                    }),
                    new SButton("Resize!")
                    {
                        OnClick = OnResizeClicked,
                        Position = new Vector2(5, 55),
                        With =
                        {
                            new WideButtonModifier(55, 5),
                        },
                    }
                },

                With =
                {
                    //new MovableModifier()
                },

                Visible = Enabled,
            });

            group.UpdateChildrenStyles();
            group.Children.ForEach(a => { group.GrowToFit(a); });
            group.UpdateStyle();
            group.WindowTitleBar.Background = new Color(0.3f, 0.3f, 0.3f, 0.7f);
            group.WindowTitleBar.Foreground = Color.white;
            group.WindowTitleBar.UpdateStyle();

            //group.Enabled = false;
            group.Hide();
        }

        static void OnResizeClicked(SButton button)
        {
            Debug.Log($"Resize clicked! Target map size: {textbox_x.Text}x{textbox_y.Text}");
            try
            {
                int x = int.Parse(textbox_x.Text);
                int y = int.Parse(textbox_y.Text);

                int xoff = (textbox_xoff.Text.Trim() == "") ? 0 : int.Parse(textbox_xoff.Text);
                int yoff = (textbox_yoff.Text.Trim() == "") ? 0 : int.Parse(textbox_yoff.Text);

                MapResizer.ResizeMap(x, y, xoff, yoff);
            }
            catch(Exception e)
            {
                Debug.Log($"Caught exception:\n{e}");
            }
        }

        public static void Init()
        {
            if (Root != null) return;
            Root = SGUIRoot.Setup();
            //GameObject.Find("SGUI Root").tag = "DoNotPause";

            InitResizeGUI();
        }

        public static void ToggleResizeMenu()
        {
            //Debug.Log($"ToggleResizeMenu; Visible: {!ResizePanel.Visible}");
            //ResizePanel.Enabled = !ResizePanel.Enabled;
            if (!Enabled) return;
            //ResizePanel.Visible = !ResizePanel.Visible;
            if (ResizePanel.Visible) ResizePanel.Hide(); else ResizePanel.Show();
        }

        public static void SetupMenu(bool enabled, int x = 0, int y = 0)
        {
            Debug.Log($"SetupMenu; Enabled: {enabled}, x: {x}, y: {y}");
            Enabled = enabled;
            if(enabled && ResizePanel != null)
            {
                textbox_x.Text = x.ToString(); textbox_y.Text = y.ToString();
                //textbox_xoff.Text = "0"; textbox_yoff.Text = "0";
            }
            if(ResizePanel != null)
                if (enabled) ResizePanel.Show(); else ResizePanel.Hide();
        }
    }
}
