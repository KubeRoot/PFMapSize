﻿using SGUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PFEditMapSize
{
    class MovableModifier : SModifier
    {
        Vector2 lastMousePos = Vector2.zero;
        bool IsDragging = false;

        public override void Update()
        {
            Vector2 mousepos = Input.mousePosition;
            mousepos.y *= -1;

            if (IsDragging)
            {
                Vector2 mdelta = mousepos - lastMousePos;
                Elem.Position += mdelta;
            }

            Event e = Event.current;
            if (e != null && e.isMouse)
            {
                if (Elem.IsHovered)
                {
                    if (e.type == EventType.MouseDown)
                        IsDragging = true;
                }
                if (IsDragging && e.type == EventType.MouseUp)
                    IsDragging = false;
            }

            lastMousePos = mousepos;
        }
    }

    class WideButtonModifier : SModifier
    {
        public float yoff = 0;
        public float margin = 5;

        public WideButtonModifier(float y, float _margin)
        {
            yoff = y;
            margin = _margin;
        }

        public override void UpdateStyle()
        {
            float parentsizex = Elem.Parent.InnerSize.x;
            Elem.Size.x = parentsizex - margin * 2;
            Elem.Position = new Vector2(margin, yoff);
        }
    }

    class Tooltip : SModifier
    {
        public readonly string Text = "";
        public float Delay = 1f;
        Vector2 lastMousePos = Vector2.zero;
        Vector2 tipMousePos = Vector2.zero;
        
        SGroup tooltip = null;

        Vector2 offset = new Vector2(5, -5);
        Vector2 size = Vector2.zero;
        float counter = 0;
        bool tipvisible = false;

        public Tooltip() { }
        public Tooltip(string text) => Text = text;
        public Tooltip(string text, float delay)
        {
            Text = text; Delay = delay;
        }

        //public override void Init()
        //{
        //    size = Elem.Backend.MeasureText(Text) + new Vector2(4, 4);
        //}

        private SGroup CreateTooltip(Vector2 pos)
        {
            if(size == Vector2.zero) size = Elem.Backend.MeasureText(Text) + new Vector2(4, 4);
            tooltip = new SGroup()
            {
                //AutoGrowDirection = SGroup.EDirection.Horizontal,
                //ContentSize = Vector2.zero,
                Background = new Color(0.7f, 0.7f, 0.7f, 0.6f),
                Position = pos - new Vector2(0, size.y),
                Size = size,
                Children =
                {
                    new SLabel(Text)
                    {
                        Alignment = TextAnchor.MiddleLeft,
                    }
                },
                With =
                {
                    new DieWithElement(Elem),
                },
            };

            //tooltip.Children.ForEach(a => tooltip.GrowToFit(a));
            //tooltip.UpdateStyle();

            return tooltip;
        }

        public override void Update()
        {
            if (!Elem.Visible)
            {
                if(tipvisible)
                {
                    tipvisible = false;
                    //tooltip.Visible = false;
                    //tooltip.Enabled = false;
                    //tooltip.Hide();
                    tooltip.Remove();
                    counter = 0;
                    Debug.Log("Hidng tooltip");
                }
                return;
            }
            Vector2 mousepos = Input.mousePosition;
            mousepos = new Vector2(
                mousepos.x,
                Elem.Root.Size.y - mousepos.y - 1f
            );
            //mousepos.y = Screen.currentResolution.height - mousepos.y;

            if(!tipvisible && Elem.IsHovered)
            {
                //Debug.Log(counter);
                counter += Time.deltaTime;
            }
            else if(!tipvisible)
            {
                counter = 0;
            }

            if (counter >= Delay && !tipvisible && mousepos.Similar(lastMousePos))
            {
                tipMousePos = mousepos;
                tipvisible = true;
                //tooltip.Visible = true;
                //tooltip.Enabled = true;
                //tooltip.Show();
                //tooltip.Update();
                CreateTooltip(mousepos+offset);
                //tooltip.Position = mousepos + offset;
                Debug.Log("Showing tooltip");
            }
            
            if(tipvisible && (mousepos-tipMousePos).magnitude > 4)
            {
                tipvisible = false;
                //tooltip.Visible = false;
                //tooltip.Enabled = false;
                //tooltip.Hide();
                tooltip.Remove();
                counter = 0;
                Debug.Log("Hidng tooltip");
            }

            lastMousePos = mousepos;
        }
    }

    class DieWithElement : SModifier
    {
        private SElement parent;

        public DieWithElement(SElement _parent) => parent = _parent;

        int counter = 0;
        int interval = 100;
        public override void Update()
        {
            counter++;
            if(counter%interval == 0)
            {
                bool fail = false;
                SElement element = parent;
                while(element.Parent != null)
                {
                    if(!element.Parent.Children.Contains(element))
                    {
                        fail = true;
                        break;
                    }

                    element = element.Parent;
                }
                if(fail || parent.Parent != null || !parent.Root.Children.Contains(element))
                {
                    Elem.Remove();
                    Debug.Log("Killing element");
                }
            }
        }
    }

    static class Extensions
    {
        public static bool Similar(this Vector2 a, Vector2 b, float eps = 0.01f)
        {
            return Math.Abs(a.x - b.x) < eps && Math.Abs(a.y - b.y) < eps;
        }

        public static void Hide(this SElement element)
        {
            //element.Enabled = false;
            element.Visible = false;
            element.Children.ForEach(a => a.Hide());
        }

        public static void Show(this SElement element)
        {
            element.Children.ForEach(a => a.Show());
            //element.Enabled = true;
            element.Visible = true;
        }
    }
}

