PFEditMapSize - Resize and shift your maps from inside the game!

Usage:
When editting a map, a window will be shown containing two sets of textboxes. The first set represents the map size, the second represents the offset.
You can toggle the window by pressing `k`.

When the Resize button is pressed, the map is resized to the specified dimensions (in grid coordinates).
Additionally, everything in the map (terrain, entities, mire) is shifted by the specified offset, allowing you to move the entire map as necessary.

Any terrain (and similar) are permanently deleted if they end up off the map (either by decreasing the map or shifting them off the edges)
Other things are generally not deleted when moved off the map.

Installation instructions:

- Download [Release.zip from the repo](https://gitlab.com/KubeRoot/PFMapSize/raw/master/Release.zip)
- Unpack into Particle Fleet Emergence\ParticleFleet_Data\Managed
- Run patch.bat